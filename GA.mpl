# GA - Geometric algebra package for Maple.
# Version 1.2.2, 28th October 2004.
#
# Changes
# -------
#
# 1.2.1: Fixed project() to work correctly on scalar arguments.
# 1.2.2: Fixed project(), correctly this time!
#        Renamed version() to ga_version().
#        Removed a number of quotes (') to get it to work with Maple 9
#        (thanks to Giovanni Vulpetti <giovanni_vulpetti@telespazio.it>) 
#
# Prints version and author information

GA[ga_version] := proc()
  print(`GA Package`);
  print(`Version 1.2.2`);
  print(`Mark Ashdown, maja1@mrao.cam.ac.uk`);
  NULL
end:

# Initialisation procedure. Defines infix operators and metrics.
# Information about metrics is stored in global variable _MetricTable
# and information about current metric is stored in global variables
# _Metric, _Sig, _MinIndex and _MaxIndex.

GA[init] := proc()
  global `&@`,`&.`,`&^`,`&x`,`&s`;
  define(`&@`, `&@`(a::algebraic, b::algebraic) = geomp(a,b));
  define(`&.`, `&.`(a::algebraic, b::algebraic) = innerp(a,b));
  define(`&^`, `&^`(a::algebraic, b::algebraic) = outerp(a,b));
  define(`&x`, `&x`(a::algebraic, b::algebraic) = commp(a,b));
  define(`&s`, `&s`(a::algebraic, b::algebraic) = scalarp(a,b));
  protect('`&@`','`&.`','`&^`','`&x`','`&s`','e');
  GA[add_metric]('default',signum,-infinity,infinity):
  GA[add_metric]('SA',proc() 1 end,1,3):
  GA[add_metric]('STA',proc(i) `if`(i=0,1,-1) end, 0,3):
  GA[add_metric]('MSTA',proc(i) `if`(modp(i,4)=0,1,-1) end, 0, infinity):
  GA[metric]('default')
end:

# Sets current metric by changing global variables _Metric, _Sig,
# _MaxIndex and _MinIndex 

GA[metric] := proc(a::name)
  global _Metric,_Sig,_MetricTable,_MinIndex,_MaxIndex;
  if member([a],[indices(_MetricTable)]) then
    unprotect('_Metric','_Sig','_MinIndex','_MaxIndex');
    _Metric := `a`;
    _Sig := _MetricTable[_Metric][`sig`];
    _MinIndex := _MetricTable[_Metric][`min`];
    _MaxIndex := _MetricTable[_Metric][`max`];
    protect('_Metric','_Sig','_MinIndex','_MaxIndex');
    print(`Metric is now set to: `||`_Metric`);
  else
    ERROR(`The metric `||`a`||` is unknown to the GA package`);  
  fi;
end:

# Adds metric to internal table _MetricTable so that it can be invoked
# with 'metric'

GA[add_metric] := proc(fred::name,signature::procedure,
			minind::{integer,infinity},maxind::{integer,infinity})
  global _MetricTable;
  unprotect('_MetricTable');
  _MetricTable['fred'][sig] := eval(signature);
  if minind > maxind then
	ERROR(`The minimum index is greater than the maximum index`) fi;
  _MetricTable['fred'][min] := minind;
  _MetricTable['fred'][max] := maxind;
  protect('_MetricTable');
  print(``||`The GA package knows about metric \'`||fred||`\'`); 
end:

# Geometric product of a and b

GA[geomp] := proc(a::algebraic,b::algebraic)
  local B,C,arga,argb,i;
  arga := expand(a);
  argb := expand(b);
  if arga::`+` then map(procname, arga,argb)
  elif argb::`+` then map2(procname,arga,argb)
  elif arga::`*` then
    coeffic(arga,'B','C');
    B*procname(C,argb)
  elif argb::`*` then
    coeffic(argb,'B','C');
    B*procname(arga,C)
  elif arga::blade then
    if argb::blade then
      bladep(arga,argb)
    else
      argb*verify_blade(arga);
    fi
  elif argb::blade then
    arga*verify_blade(argb)
  else
    arga*argb;
  fi
end:

# Geometric product between two blades a and b - the most fundamental
# routine in the package - used in geomp, innerp and outerp

GA[bladep] := proc(b::blade,c::blade)
  global _Sig;
  local A,B,C,count,p;
  A := [];
  B := [op(verify_blade(b))];
  C := [op(verify_blade(c))];
  count := 0;
  p := 1;
  while not(B=[] and C=[]) do
    if B=[] then
      A := [op(A),op(C)];
      C := [];
    elif C=[] then
      A := [op(A),op(B)];
      B := [];
    elif B[1]<C[1] then
      A := [op(A),B[1]];
      B := subsop(1=NULL,B);
    elif C[1]<B[1] then
      A := [op(A),C[1]];
      count := (count+nops(B)) mod 2;
      C := subsop(1=NULL,C)
    else
      count := (count+nops(B)-1) mod 2;
      p := p*_Sig(B[1]);
      B := subsop(1=NULL,B);
      C := subsop(1=NULL,C)
    fi
  od;
  if A=[] then A := 1 else A := e[op(A)] fi;
  (-1)^count*p*A
end:

# Inner product of a and b

GA[innerp] := proc(a::algebraic,b::algebraic)
  local B,C,arga,argb,i;
  arga := expand(a);
  argb := expand(b);
  if arga::`+` then map(procname,arga,argb)
  elif argb::`+` then map2(procname,arga,argb)
  elif arga::`*` then
    coeffic(arga,'B','C');
    B*procname(C,argb);
  elif argb::`*` then
    coeffic(argb,'B','C');
    B*procname(arga,C);
  elif [arga,argb]::[blade,blade] then
    project(bladep(arga,argb),abs(nops(arga)-nops(argb)))
  else 0
  fi
end:

# Outer product of a and b

GA[outerp] := proc(a::algebraic,b::algebraic)
  local B,C,arga,argb,i;
  arga := expand(a);
  argb := expand(b);
  if arga::`+` then map(procname,arga,argb)
  elif argb::`+` then map2(procname,arga,argb)
  elif arga::`*` then
    coeffic(arga,'B','C');
    B*procname(C,argb);
  elif argb::`*` then
    coeffic(argb,'B','C');
    B*procname(arga,C);
  elif arga::blade then
    if argb::blade then
      project(bladep(arga,argb),nops(arga)+nops(argb))
    else
      argb*verify_blade(arga)
    fi
  elif argb::blade then
    arga*verify_blade(argb);
  else 
    arga*argb; 
  fi;
end:

# Defines the type 'blade' as anything called 'e' with indices

GA[`type/blade`] := proc(a)
  type(a,indexed) and op(0,a)='e';
end:

# Checks blade to see if indices are integers in ascending order and within
# the permitted range for the current metric, if not throws error

GA[verify_blade] := proc(a::blade)
  local i;
  global _Metric,_MinIndex,_MaxIndex;
  for i from 1 to nops(a) do 
    if not(op(i,a)::integer) or (i<>1 and op(i,a)<=op(i-1,a)) then
      ERROR(`indices of blade`,a,`must be integers arranged in ascending \
order, with no repeats`);
    fi;
  od; 
  if op(1,a)<_MinIndex or op(-1,a)>_MaxIndex then
    ERROR(`index in blade`,a,`outside permitted range for metric`,`_Metric`);  
  fi;
  a;
end:

# Takes expressions a like scalar*blade, throws error if blades have been
# multiplied by `*` and returns its coefficient and blade part in C and B
# respectively

GA[coeffic] := proc(a::algebraic,C,B)
  local arg,A;
  arg := expand(a);
  if arg::`*` then
    C := remove(type,arg,blade);
    A := select(type,arg,blade);
    if A::`*` then
      ERROR(`multivectors cannot be multiplied with *`)
    fi;
    B := A;
  else 1
  fi  
end:

# Reverses a 

GA[reverse] := proc(a::algebraic)
  local arg,A,B;
  arg := expand(a);
  if arg::`+` then map(procname,arg)
  elif arg::`*` then
    coeffic(arg,'A','B');
    if B=1 then arg else A*procname(B) fi
  elif arg::blade and modp(nops(arg),4) > 1 then
    -arg
  else arg
  fi
end:

# Returns set of grades contained in algebraic expression a

GA[grade] := proc(a::algebraic)
  local arg,i,A,B;
  arg := expand(a);
  if arg::`+` then
    `union`(seq(procname(ai),ai=arg))
  elif a::`*` then
    coeffic(arg,'A','B');
    if B=1 then {0} else {nops(B)} fi
  elif arg::blade then
    {nops(arg)}
  elif arg=0 then 
    {}
  else {0}
  fi
end:

# Projects out objects of grade or set of grades r from expression a

GA[project] := proc(a::algebraic,g::{nonnegint,set(nonnegint)})
  local arg,i,A,B;
  arg := expand(a);
  if g::set then
    add(project(arg,i),i=g)
  elif arg::`+` then
    map(procname,arg,g);
  elif arg::`*` then
    coeffic(arg,'A','B');
    if (B=1) then
      if (g=0) then arg else 0 fi
    elif (nops(B)=g) then
      arg
    else
      0
    fi
  elif arg::blade then
    if nops(arg)=g then arg else 0 fi
  elif g=0 then
    arg
  else
    0
  fi
end:

# Returns set of blades contained in expression a

GA[contains] := proc(a::algebraic)
  local arg,ai,A,B;
  arg := expand(a);
  if arg::`+` then `union`(seq(procname(ai),ai=arg))
  elif arg::`*` then
    coeffic(arg,'A','B');
    {B};
  elif arg::blade then
    {arg}
  else
    {1}
  fi
end:

# Multivector derivative of a with respect to x

GA[mderiv] := proc(a::algebraic,x::algebraic)
  local A,B,C,i;
  A := 0;
  for i in contains(x) do
    B := project(geomp(i,x),0);
    if B<>0 then
      C := sign(B);
      A := A + C*geomp(i,diff(a,C*B))
    fi
  od;
  A
end:

# Multivector directional derivative of a with respect to x
# in direction of b

GA[mdiff] := proc(a::algebraic,x::algebraic,b::algebraic)
  local A,B,C,i;
  A := 0;
  for i in contains(x) do
    B := project(geomp(x,i),0);
    if B<>0 then
      C := sign(B);
      A := A + project(geomp(b,i),0)*C*diff(a,C*B)
    fi
  od;
  A
end:

# Collects the expression a into sum of coefficient*blade

GA[cc] := proc(a::algebraic)
  collect(expand(a),`minus`(contains(a),{1}));
end:

# Multivector exponential of expression a using simple approach

GA[mexp] := proc(a::algebraic)
  local A,B;
  B := geomp(a,a);
  if B=project(B,0) then
    if B = 0 then
      1 + a
    elif sign(B)=1 then
      A := sqrt(B,symbolic);
      cosh(A)+(a/A)*sinh(A)
    elif sign(B)=-1 then
      A := sqrt(-B,symbolic);
      cos(A)+(a/A)*sin(A)
    else 
      ERROR(`something has gone wrong`)
    fi
  else
    ERROR(`exponent too complicated`)
  fi
end:

# n-th power of a

GA[mpow] := proc(a::algebraic,n::nonnegint)
  local A;
  A := 1;
  to n do
    A := geomp(A,a)
  od;
  A
end:

# Commutator product of a and b

GA[commp] := proc(a::algebraic,b::algebraic)
  1/2*geomp(a,b)-1/2*geomp(b,a);
end:

# Scalar product of a and b

GA[scalarp] := proc(a::algebraic,b::algebraic)
  project(geomp(a,b),0);
end:

# Creates multivector with name x of with grade or set of grades g with
# indices ind

GA[mv] := proc(x::name,g::{nonnegint,set(nonnegint)},
			ind::set(integer))
  local i;
  if g::set then
    add(procname(x,i,ind), i=g)
  elif g > nops(ind) then
    ERROR(`grade too large for number of indices.`)
  elif g=0 then
    x[NULL]
  else
    add(x[op(i)]*e[op(i)], i=combinat[choose](sort([op(ind)]),g))
  fi
end:

# 'Slides' the indices of blades in expression a up or down by s

GA[slide] := proc(a::algebraic,s::integer)
  local arg,A,B;
  arg := expand(a);
  if arg::`+` then
    map(procname,a,s)
  elif arg::`*` then
    coeffic(arg,'A','B');
    if B=1 then arg else A*procname(B,s) fi
  elif arg::blade then
    e[op(map(`+`,[op(arg)],s))]
  else
    arg
  fi
end:

# Outermorphism of vector function f(x) acting on a

GA[outerm] := proc(a::algebraic,f::procedure,x::algebraic)
  local arg,i,A,B,g;
  arg := expand(a);
  if arg::`+` then map(procname,arg,f)
  elif a::`*` then
    coeffic(arg,'A','B');
    if B=1 then arg else A*procname(B,f) fi
  elif a::blade then
    g := b->mdiff(f(x),x,b);
    A := 1;    
    for i in [op(arg)] do
      A := geomp(A,g(e[i]))
    od; 
    project(A,nops(arg))
  else
    arg
  fi
end:
