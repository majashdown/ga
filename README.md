
GA Package for Maple
====================

Mark Ashdown \<maja1@cam.ac.uk\>

Astrophysics Group, Cavendish Laboratory, University of Cambridge.

This software is provided "as is" with no guarantee that it will
work. I wrote it during the course of my research because I needed to
perform calculations in geometric/Clifford algebras of arbitrary
dimensions and signatures and I found that the existing packages were
too cumbersome or limited. In the process of writing this package and
using it, I have tried to eliminate as many bugs as possible. I hope
you will find it useful.

Update on 4th April 2003: The package was originally written for Maple
V. I have tried to update the source to be compatible with more recent
versions. If you find any bugs, report them to me (or even better, fix
them and send a fixed version of the source to me!).

Update on 28th October 2004: I have attempted to update the package
for Maple 9.

You should be able to find the latest version of this package at:
http://gitlab.com/majashdown/ga/.

The files in the archive should be:

- `README.md` - this file
- `LICENCE.md` - licence
- `GA.mpl` - source file for GA package
- `GAhelp.mws` - Old-style Maple worksheet giving basic instructions for the
  package
- `GAhelp.mw` - New-style Maple worksheet giving basic instructions for the
  package

How to load the GA package
--------------------------

Start a Maple session and type

    read `GA.mpl`;

If Maple cannot find the file, you may have to prefix the filename
with the full directory path. The package is loaded by issuing the
command

    with(GA);

which should produce a list of the procedures in the package.

If you want to keep using this package, on Unix/Linux systems you can
create or alter the .mapleinit file in your home directory to contain
the line

    libname := `PATH`,libname:

where PATH is the full directory path to the directory in which you
have saved the files. This will tell Maple to look in this directory
as well as in the default directories when asked to find a package.

How to use the GA package
-------------------------

Instructions for using the package are contained in the worksheets
`GAhelp.mws` and `GAhelp.mw`.
